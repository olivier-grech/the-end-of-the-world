export class TheEndOfTheWorldItemSheet extends ItemSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["the-end-of-the-world", "sheet", "item"],
    });
  }

  get template() {
    return `systems/the-end-of-the-world/templates/the-end-of-the-world-${this.item.type}-sheet.html`;
  }

  /** @override */
  getData() {
    const data = super.getData();
    const itemData = data.item;
    data.system = itemData.system;
    return data;
  }
}
