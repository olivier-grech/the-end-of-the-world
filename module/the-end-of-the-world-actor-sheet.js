export class TheEndOfTheWorldActorSheet extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["the-end-of-the-world", "sheet", "actor"],
      width: 600,
      height: 800,
    });
  }

  get template() {
    return `systems/the-end-of-the-world/templates/the-end-of-the-world-${this.actor.type}-sheet.html`;
  }

  /** @override */
  getData() {
    const data = super.getData();
    const actorData = this.actor.toObject(false);
    data.system = actorData.system;

    if (actorData.type == "character") {
      this._prepareCharacterItems(data);
    }

    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    if (!this.options.editable) {
      return;
    }

    // Add item
    html.find(".item-create").click(this._onItemCreate.bind(this));

    // Edit item
    html.find(".item-edit").click((ev) => {
      const boxItem = $(ev.currentTarget).parents(".cell");
      const item = this.actor.items.get(boxItem.data("itemId"));
      item.sheet.render(true);
    });

    // Delete item
    html.find(".item-delete").click((ev) => {
      const boxItem = $(ev.currentTarget).parents(".cell");
      const item = this.actor.items.get(boxItem.data("itemId"));
      item.delete();
    });

    // Roll
    html.find(".roll-characteristic").click((event) => {
      let characteristic_key = event.currentTarget.dataset.characteristic;
      let categorie_key = "";

      if (
        characteristic_key == "dexterity" ||
        characteristic_key == "vitality"
      ) {
        categorie_key = "physical";
      }
      if (characteristic_key == "logic" || characteristic_key == "willpower") {
        categorie_key = "mental";
      }
      if (characteristic_key == "charisma" || characteristic_key == "empathy") {
        categorie_key = "social";
      }

      let characteristic =
        this.actor.system.categories[categorie_key].characteristics[
          characteristic_key
        ];
      this._openActionRollPopup(characteristic);
    });

    for (let categorie in this.actor.system.categories) {
      html
        .find("#" + categorie + "-stress-gauge-click-target")
        .click(() => this._increaseStress(categorie));
      html
        .find("#" + categorie + "-stress-gauge-click-target")
        .contextmenu(() => this._decreaseStress(categorie));
    }
  }

  _increaseStress(categorie) {
    var updatedValue = Math.min(
      this.actor.system.categories[categorie].stress + 1,
      9
    );
    var updateObject = {};
    updateObject.system = {};
    updateObject.system.categories = {};
    updateObject.system.categories[categorie] = {};
    updateObject.system.categories[categorie].stress = updatedValue;
    this.actor.update(updateObject);
  }

  _decreaseStress(categorie) {
    var updatedValue = Math.max(
      this.actor.system.categories[categorie].stress - 1,
      0
    );
    var updateObject = {};
    updateObject.system = {};
    updateObject.system.categories = {};
    updateObject.system.categories[categorie] = {};
    updateObject.system.categories[categorie].stress = updatedValue;
    this.actor.update(updateObject);
  }

  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const type = header.dataset.type;
    const categorie = header.dataset.categorie;
    const data = duplicate(header.dataset);
    const name = `New ${type.capitalize()}`;
    const itemData = {
      name: name,
      type: type,
      data: data,
    };

    if (categorie) {
      itemData.system = {};
      itemData.system.categorie = categorie;
    }

    delete itemData.data["type"];
    return this.actor
      .createEmbeddedDocuments("Item", [itemData])
      .then((items) => items[0].sheet.render(true));
  }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;
    const equipments = [];
    const features = [];
    const traumas = [];

    for (let i of sheetData.items) {
      i.img = i.img || DEFAULT_TOKEN;

      if (i.type === "equipment") {
        equipments.push(i);
      } else if (i.type === "feature") {
        features.push(i);
      } else if (i.type === "trauma") {
        traumas.push(i);
      }
    }

    actorData.equipments = equipments;
    actorData.features = features;
    actorData.traumas = traumas;
  }

  async theEndOfTheWorldRoll(
    positive_dice_count,
    negative_dice_count,
    actor_name,
    characteristic_label
  ) {
    let positive_roll = new Roll(`${positive_dice_count}d6[white]`, {});
    let negative_roll = new Roll(`${negative_dice_count}d6[black]`, {});

    await positive_roll.roll();
    await negative_roll.roll();

    let positive_result_list = positive_roll.dice[0].results.map((x) => {
      return { result: x.result, valid: true };
    });
    let negative_result_list = negative_roll.dice[0].results.map((x) => {
      return { result: x.result, valid: true };
    });

    for (let positive_result of positive_result_list) {
      for (let negative_result of negative_result_list) {
        if (
          negative_result.valid &&
          positive_result.result == negative_result.result
        ) {
          positive_result.valid = false;
          negative_result.valid = false;
          break;
        }
      }
    }

    let positive_result_valid = [];
    let positive_result_cancelled = [];
    let negative_result_valid = [];
    let negative_result_cancelled = [];

    for (let positive_result of positive_result_list) {
      if (positive_result.valid) {
        positive_result_valid.push(positive_result.result);
      } else {
        positive_result_cancelled.push(positive_result.result);
      }
    }

    for (let negative_result of negative_result_list) {
      if (negative_result.valid) {
        negative_result_valid.push(negative_result.result);
      } else {
        negative_result_cancelled.push(negative_result.result);
      }
    }

    positive_result_valid.sort();
    positive_result_cancelled.sort();
    negative_result_valid.sort();
    negative_result_cancelled.sort();

    this._showRollMessage(
      positive_result_valid,
      positive_result_cancelled,
      negative_result_valid,
      negative_result_cancelled,
      positive_roll,
      negative_roll,
      actor_name,
      characteristic_label
    );
  }

  // Find the checkbox/radio checked with the given name
  findChecked(html, name) {
    let elements = html.find(`[name="${name}"]`);
    for (let element of elements) {
      if (element.checked) {
        return element.value;
      }
    }
  }

  async _openActionRollPopup(characteristic) {
    let content = await renderTemplate(
      "systems/the-end-of-the-world/templates/the-end-of-the-world-roll-popup.html",
      this.actor.toObject(false)
    );
    new Dialog({
      title: game.i18n.localize(characteristic.label),
      content: content,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: "Roll",
          callback: (html) => {
            let positive_dice_count = this.findChecked(html, "roll-positive");
            let negative_dice_count = this.findChecked(html, "roll-negative");
            let actor_name = this.actor.name;
            this.theEndOfTheWorldRoll(
              positive_dice_count,
              negative_dice_count,
              actor_name,
              characteristic.label
            );
          },
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize("Close"),
        },
      },
    }).render(true);
  }

  async _showRollMessage(
    positive_result_valid,
    positive_result_cancelled,
    negative_result_valid,
    negative_result_cancelled,
    positive_roll,
    negative_roll,
    actor_name,
    characteristic_label
  ) {
    let speaker = ChatMessage.getSpeaker();

    let result = await renderTemplate(
      "systems/the-end-of-the-world/templates/the-end-of-the-world-roll-result.html",
      {
        actor_name: actor_name,
        positive_result_valid: positive_result_valid,
        positive_result_cancelled: positive_result_cancelled,
        negative_result_valid: negative_result_valid,
        negative_result_cancelled: negative_result_cancelled,
        characteristic_label: characteristic_label,
      }
    );

    let messageData = {
      speaker: speaker,
      content: result,
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      rolls: [positive_roll, negative_roll],
    };

    ChatMessage.create(messageData);
  }
}
