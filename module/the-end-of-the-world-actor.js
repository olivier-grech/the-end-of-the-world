export class TheEndOfTheWorldActor extends Actor {
  /** @override */
  async _preCreate() {
    for (let i = 0; i < 3; i++) {
      let newTrauma = await Item.create({
        name: "New Trauma",
        type: "trauma",
        system: {
          categorie: "physical",
          type: "noTrauma",
        },
      });
      this.items.set(i, newTrauma);
    }
    for (let i = 3; i < 6; i++) {
      let newTrauma = await Item.create({
        name: "New Trauma",
        type: "trauma",
        system: {
          categorie: "mental",
          type: "noTrauma",
        },
      });
      this.items.set(i, newTrauma);
    }

    for (let i = 6; i < 9; i++) {
      let newTrauma = await Item.create({
        name: "New Trauma",
        type: "trauma",
        system: {
          categorie: "social",
          type: "noTrauma",
        },
      });
      this.items.set(i, newTrauma);
    }
  }
}
