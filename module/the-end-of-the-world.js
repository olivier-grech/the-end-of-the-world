import { TheEndOfTheWorldActorSheet } from "./the-end-of-the-world-actor-sheet.js";
import { TheEndOfTheWorldActor } from "./the-end-of-the-world-actor.js";

import { TheEndOfTheWorldItemSheet } from "./the-end-of-the-world-item-sheet.js";

Hooks.once("init", async function () {
  console.log(`Initializing The End of the World system`);

  CONFIG.Actor.documentClass = TheEndOfTheWorldActor;

  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("the-end-of-the-world", TheEndOfTheWorldActorSheet, {
    types: ["character"],
    makeDefault: true,
  });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("the-end-of-the-world", TheEndOfTheWorldItemSheet, {
    types: ["equipment", "feature", "trauma"],
    makeDefault: true,
  });

  const templatePaths = [
    "systems/the-end-of-the-world/templates/the-end-of-the-world-character-sheet.html",
    "systems/the-end-of-the-world/templates/the-end-of-the-world-equipment-sheet.html",
    "systems/the-end-of-the-world/templates/the-end-of-the-world-feature-sheet.html",
    "systems/the-end-of-the-world/templates/the-end-of-the-world-trauma-sheet.html",
  ];
  loadTemplates(templatePaths);
});

Handlebars.registerHelper("signedNumber", function (number) {
  if (number > 0) {
    return "+" + number;
  } else if (number < 0) {
    return number;
  }
  return number;
});

Handlebars.registerHelper("positiveNegativeDiceCount", function (diceCount) {
  if (diceCount >= 0) {
    return diceCount + " Positive Dice";
  }
  return -diceCount + " Negative Dice";
});

Handlebars.registerHelper("ifEquals", function (arg1, arg2, options) {
  return arg1 == arg2 ? options.fn(this) : options.inverse(this);
});

Handlebars.registerHelper(
  "emptyStressTick",
  function (categorie, lowValue, highValue, block) {
    let stressLevel = categorie["stress"];
    let result = "";
    for (let i = lowValue; i <= highValue; i++) {
      if (stressLevel < i) {
        result += block.fn(i);
      }
    }
    return result;
  }
);

Handlebars.registerHelper(
  "filledStressTick",
  function (categorie, lowValue, highValue, block) {
    let stressLevel = categorie["stress"];
    let result = "";
    for (let i = lowValue; i <= highValue; i++) {
      if (stressLevel >= i) {
        result += block.fn(i);
      }
    }
    return result;
  }
);

Handlebars.registerHelper(
  "diceCount",
  function (categorie, lowValue, highValue, block) {
    let stressLevel = categorie["stress"];
    let result = "";
    for (let i = lowValue; i <= highValue; i++) {
      if (stressLevel >= i) {
        result += block.fn(i);
      }
    }
    return result;
  }
);
