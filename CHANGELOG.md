# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.0] - 2023-08-29
### Added
- Cancelled dice are now indicated in roll messages.

### Fixed
- Fix a typo in a translation entry.

## [1.3.0] - 2023-08-29
### Added
- Rolls are now passed to the corresponding chat message, allowing Dice so Nice integration among other things.

### Changed
- Use SCSS internally.
- Make some minor style adjustments.

## [1.2.0] - 2023-08-22
### Added
- French translation.
- Hover effect for dice selector.

### Fixed
- Fix a typo in a translation entry.

## [1.1.0] - 2023-08-21
### Added
- Automated rolls.

### Changed
- Change sheet style.

## [1.0.0] - 2023-05-04
### Added
- Initial version.
