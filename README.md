# Foundry Virtual Tabletop The End of the World System

This package for [Foundry Virtual Tabletop](https://foundryvtt.com/) lets you play the role-playing game [The End of the World](https://www.fantasyflightgames.com/en/news/2016/8/19/its-the-end-of-the-world/), originally published by Fantasy Flight Games (the English version of the game is no longer published at the moment).

**This package is fan-made and is not affiliated with Fantasy Flight Games.**

You can support me if you like my work:

[![Buy Me a Coffee](./bmc-button.svg)](https://www.buymeacoffee.com/oliviergrech)

## Features

- Character sheet.
- Automated roll system. Click on a characteristic name to open a roll dialog.

## Screenshots

![Screenshot](screenshots/screenshot.jpg)
